extern crate actix_web;
use actix_web::{server, App, HttpRequest, Responder};

pub mod http_server;
pub mod domain;
pub mod interface;
pub mod usecase;
pub mod manager;

fn greet(req: &HttpRequest) -> impl Responder {
    let to = req.match_info().get("name").unwrap_or("World");
    format!("Hello {}!", to)
}

fn main() {
	let server_address = String::from("127.0.0.1:8000");
	println!("Running server on: {}...", server_address);

    server::new(|| {
        App::new()
            .resource("/", |r| r.f(greet))
            .resource("/{name}", |r| r.f(greet))
    })
    .bind(server_address)
    .expect("Can not bind to port 8000")
    .run();
}
