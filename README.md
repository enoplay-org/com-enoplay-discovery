# Enoplay Data Discovery Service

`com-enoplay-discovery` is an http server for discovering data on the Enoplay game network. This service provides data about where to find data, rather than the data itself.

For example, if a user is searching for a particular game, this service knows about the title, description and a few other searchable meta-data; however, relies on links such as the `dat://` archive url to provide the full record of details such as app url, thumbnails, screenshots, play controls, etc.

## License
MIT
